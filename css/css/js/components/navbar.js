$(function () {
    var $navbar = $('.navbar');

    $navbar.find('.navbar-search-field').hide();

    $navbar.find('.open-navbar-search').on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var _self = $(this);
        var target = _self.closest($navbar).find('.navbar-search-field');

        target.toggle();

        target.on('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
        });

    });

    $navbar.find('.navbar-hamburger').on('click', function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($navbar).find('.collapse');

        target.toggle();

        if (target.is(":visible")) {
            $('html').css('overflowY', 'hidden');
        } else {
            $('html').css('overflowY', 'auto');
        }
    });

    $navbar.find('.dropdown > a').on('click', function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest('.dropdown').find('.dropdown-content');
        target.toggle();
    });

    $navbar.find('.dropdown > a').on('mouseenter', function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest('.dropdown').find('.dropdown-content');
        target.show();
    });
    
    $(document).click(function () {
        $navbar.find('.navbar-search-field').hide();
    });
    
});