$(function () {
    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }
    $('#pagecontent').find('.visual').insertAfter($('.navbar')); // set visual to visual content in header

    $('#pagecontent').find('.header-full-width').insertAfter($('.navbar'));

});

