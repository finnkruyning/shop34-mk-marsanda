﻿var matched, browser;

jQuery.uaMatch = function( ua ) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
        /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
        /(msie) ([\w.]+)/.exec( ua ) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
        [];

    return {
        browser: match[ 1 ] || "",
        version: match[ 2 ] || "0"
    };
};

matched = jQuery.uaMatch( navigator.userAgent );
browser = {};

if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if ( browser.chrome ) {
    browser.webkit = true;
} else if ( browser.webkit ) {
    browser.safari = true;
}

jQuery.browser = browser;
(function(a){a.fn.superfish=function(h){var k=a.fn.superfish,f=k.c,d=a(['<span class="',f.arrowClass,'"> <img src="/img/arrow-menu.png" alt=""></span>'].join("")),j=function(){var c=a(this),l=g(c);clearTimeout(l.sfTimer);c.showSuperfishUl().siblings().hideSuperfishUl()},i=function(){var c=a(this),l=g(c),m=k.op;clearTimeout(l.sfTimer);l.sfTimer=setTimeout(function(){m.retainPath=(a.inArray(c[0],m.$path)>-1);c.hideSuperfishUl();if(m.$path.length&&c.parents(["li.",m.hoverClass].join("")).length<1){j.call(m.$path)}},m.delay)},g=function(c){var l=c.parents(["ul.",f.menuClass,":first"].join(""))[0];k.op=k.o[l.serial];return l},e=function(c){c.addClass(f.anchorClass).append(d.clone())};return this.each(function(){var m=this.serial=k.o.length;var l=a.extend({},k.defaults,h);l.$path=a("li."+l.pathClass,this).slice(0,l.pathLevels).each(function(){a(this).addClass([l.hoverClass,f.bcClass].join(" ")).filter("li:has(ul)").removeClass(l.pathClass)});k.o[m]=k.op=l;a("li:has(ul)",this)[(a.fn.hoverIntent&&!l.disableHI)?"hoverIntent":"hover"](j,i).each(function(){if(l.autoArrows){e(a(">a:first-child",this))}}).not("."+f.bcClass).hideSuperfishUl();var c=a("a",this);c.each(function(o){var n=c.eq(o).parents("li");c.eq(o).focus(function(){j.call(n)}).blur(function(){i.call(n)})});l.onInit.call(this)}).each(function(){var c=[f.menuClass];if(k.op.dropShadows&&!(a.browser.msie&&a.browser.version<7)){c.push(f.shadowClass)}a(this).addClass(c.join(" "))})};var b=a.fn.superfish;b.o=[];b.op={};b.IE7fix=function(){var c=b.op;if(a.browser.msie&&a.browser.version>6&&c.dropShadows&&c.animation.opacity!=undefined){this.toggleClass(b.c.shadowClass+"-off")}};b.c={bcClass:"sf-breadcrumb",menuClass:"sf-js-enabled",anchorClass:"sf-with-ul",arrowClass:"sf-sub-indicator",shadowClass:"sf-shadow"};b.defaults={hoverClass:"sfHover",pathClass:"overideThisToUse",pathLevels:1,delay:800,animation:{opacity:"show"},speed:"normal",autoArrows:true,dropShadows:true,disableHI:false,onInit:function(){},onBeforeShow:function(){},onShow:function(){},onHide:function(){}};a.fn.extend({hideSuperfishUl:function(){var e=b.op,d=(e.retainPath===true)?e.$path:"";e.retainPath=false;var c=a(["li.",e.hoverClass].join(""),this).add(this).not(d).removeClass(e.hoverClass).find(">ul").hide().css("visibility","hidden");e.onHide.call(c);return this},showSuperfishUl:function(){var d=b.op,e=b.c.shadowClass+"-off",c=this.addClass(d.hoverClass).find(">ul:hidden").css("visibility","visible");b.IE7fix.call(c);d.onBeforeShow.call(c);c.animate(d.animation,d.speed,function(){b.IE7fix.call(c);d.onShow.call(c)});return this}})})(jQuery);