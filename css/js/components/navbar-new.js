$(function () {
    var $navbar = $('.navbar');

    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }

    $navbar.find('.open-navbar-search').on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var _self = $(this);
        _self.toggle();
        var target = _self.closest($navbar).find('.navbar-search-field');

        target.toggle();

    });

    $navbar.find('.navbar-hamburger').on('click', function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($navbar).find('.collapse');

        target.toggle();

        if (target.is(":visible")) {
            $('html').css('overflowY', 'hidden');
        } else {
            $('html').css('overflowY', 'auto');
        }
    });

    if($('.topbar-basket-mob').length) {
        $('.topbar-basket-mob').click(function () {
            $('.topbar-basket-menu-mob').toggle('slow');
        })
    }

    $( window ).load(function() {
        if($('.topbar-basket-mob').length) {
            if($('#navbar-basket').text().trim() == '0 items') {
                $('.topbar-basket-menu-mob .basket-items-count').text('Je winkelmandje is leeg');
            } else if($('#navbar-basket').text().trim().substring(0,2) == '1 ') {
                $('.topbar-basket-menu-mob .basket-items-count').text('1 item in je winkelmandje');
            } else {
                $('.topbar-basket-menu-mob .basket-items-count').text($('#navbar-basket').text()+'in je winkelmandje');
            }
        }
    });

    $navbar.find('.dropdown > a').on('click', function (e) {
        e.preventDefault();
        var _self = $(this);
        _self.closest('.dropdown').toggleClass('active');
        var target = _self.closest('.dropdown').find('.dropdown-content');
        target.toggle();
    });

    $navbar.find('.dropdown').on('mouseleave', function (e) {
        e.preventDefault();
        var _self = $(this);
        _self.closest('.dropdown').removeClass('active');
        var target = _self.closest('.dropdown').find('.dropdown-content');
        target.hide();
    });

    if ($(window).width() < 767) {

        $(window).scroll(function () {
            if ($(document).scrollTop() > 50) {
                $('.navbar .navbar-search-field').addClass('search-field-show');
                $('.navbar').addClass('navbar-collapsed');
            } else {
                $('.navbar .navbar-search-field').removeClass('search-field-show');
                $('.navbar').removeClass('navbar-collapsed');
            }
        });
    };

});
