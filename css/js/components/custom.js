$(function () {
    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }
    $('#pagecontent').find('.visual').insertAfter($('.logo-mobile')); // set visual to visual content in header

    // move sidebar-footer to be footer of sidebar
    $('#sidebar .box.box-primary').appendTo('#sidebar');

//Search function
    $('a.zoek').on('click', function () {
        $(this).toggleClass('activeS');
        $(this).toggleClass('active').parent().find('form input').fadeToggle(function () {
            $('#search_content input').focus()
        });

    });

    // add icons for account page
    $("#body_account .button-block").each(function(){
       $(this).find(".button-block-title").prepend('<i class="fa">&nbsp;</i>');
        var hrefFA = $(this).find("a").attr("href");
        var $classFA = $(this).find(".fa");
        switch (hrefFA){
            case "/account_settings":
                $classFA.addClass("fa-cogs");
                break;
            case "account_orders":
                $classFA.addClass("fa-truck");
                break;
            case "/account_address":
                $classFA.addClass("fa-book");
                $(this).offsetParent().remove();
                break;
            case "/eigen_collectie":
                $classFA.addClass("fa-heart");
                break;
            case "/account_adres_import":
                $classFA.addClass("fa-sign-in");
                $(this).offsetParent().remove();
                break;
        }
    });
    if ($('#body_account_settings').length) {
        $('#body_account_settings .list.list-vertical li a').each(function(){
            var hrefFA = $(this).attr("href");
            switch (hrefFA){
                case "/account_adresboek":
                    $(this).parent().remove();
                    break;
                case "/account_adres_import":
                    $(this).parent().remove();
                    break;
            }
        });
    }

    // add font awesome for step button on edit-page
    $("#body_edit, #body_voorbeeld, #body_keuze").find("#nav_buttons a").append('<i class="fa">&nbsp;</i>');

    $(".accordion-body ul li a[href='" + window.location.pathname + "']").addClass('active').parents('.accordion-body').addClass('show').prev('.accordion-button').addClass('active');
    $(".middlebar .middlebar-menu-right li a[href='" + window.location.pathname + "']").addClass('active');

    if ($('#body_design').length) {
        $('.accordion.first').find('.accordion-button .icon').removeClass('icon-chevron-right').addClass('icon-chevron-down');
        $('.accordion.first').find('.accordion-body').addClass('show');

        $('.save_user_design').parent().find('i').hide();

        $('.accordion').find('.meerPrijsLink').attr('href','/kaartprijzen');
    }

    if ($('#body_design').length) {
        var $text = '<div class="desUsp"><a href="/levertijd" title="Voor 18:00 besteld, morgen in huis (ma-vr)">Voor 18:00 besteld, morgen in huis (ma-vr)</a> <a href="/papiersoorten" title="5 luxe papiersoorten">5 luxe papiersoorten</a> <a href="/enveloppen-info" title="17 kleuren enveloppen">17 kleuren enveloppen</a> <a href="#" title="We helpen u graag">We helpen u graag</a></div>';
        $('#pagecontent .box-primary .box-footer').append($text);
    }

    //Add class cardrail
    $('#card_previews_horizontal').parent().addClass('cardrail');

    //Tablet filter functionaliteiten
    if ($('.filCTA').length && $('.filCTA').is(':visible')) {
        $('#sidebar nav').prependTo('body').addClass('mobFilter');

        $('a.filCTA').on('click', function (e) {
            e.preventDefault();
            $('div.filCTAwrp').toggleClass('active');
            $('.mobFilter').toggleClass('active');
            $('.filRes em').html($('#card_previews>div').length);
            $('body').toggleClass('noflow');
        });

        $('li.tag-li').on('click', function () {
            setTimeout(function () {
                $('.filRes em').html($('#card_previews>div').length);
            }, 500);
        });

        $('.filRes').on('click', function () {
            $('div.filCTAwrp').toggleClass('active');
            $('.mobFilter').toggleClass('active');
            $('body').toggleClass('noflow');
        });
    }

    $('body').has('#full-page').addClass("full-page");

    if($('#body_design')) {
        $('.price-info a[href="/prijzen"]').attr('href','/kaartprijzen');
    }
});

